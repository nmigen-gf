from nmigen_gf.reference.state import ST


def gfpmsubr(a, b, c):
    return (c - a * b) % ST.GFPRIME
