# SPDX-License-Identifier: LGPL-3-or-later
# Copyright 2022 Jacob Lifshay programmerjake@gmail.com

# Funded by NLnet Assure Programme 2021-02-052, https://nlnet.nl/assure part
# of Horizon 2020 EU Programme 957073.

""" Carry-less Multiplication.

https://bugs.libre-soc.org/show_bug.cgi?id=784
"""

import operator
from nmigen.hdl.ir import Elaboratable
from nmigen.hdl.ast import Signal, Repl
from nmigen.hdl.dsl import Module
from nmutil.util import treereduce


class CLMulAdd(Elaboratable):
    """Carry-less multiply-add. (optional add)

        Computes:
        ```
        self.output = (clmul(self.factor1, self.factor2) ^ 
                       self.terms[0] ^ 
                       self.terms[1] ^ 
                       self.terms[2] ...)
        ```

        Properties:
        factor_width: int
            the bit-width of `factor1` and `factor2`
        term_widths: tuple[int, ...]
            the bit-width of each Signal in `terms`
        factor1: Signal of width self.factor_width
            the first input to the carry-less multiplication section
        factor2: Signal of width self.factor_width
            the second input to the carry-less multiplication section
        terms: tuple[Signal, ...]
            inputs to be carry-less added (really XOR)
        output: Signal
            the final output
    """

    def __init__(self, factor_width, term_widths=()):
        assert isinstance(factor_width, int) and factor_width >= 1
        self.factor_width = factor_width
        self.term_widths = tuple(map(int, term_widths))

        # build Signals
        self.factor1 = Signal(self.factor_width)
        self.factor2 = Signal(self.factor_width)

        # build terms at requested widths (if any)
        self.terms = []
        for i, inp in enumerate(self.term_widths):
            self.terms.append(Signal(inp, name=f"term_{i}"))

        # build output at the maximum bit-width covering all inputs
        self.output = Signal(max((self.factor_width * 2 - 1,
                                  *self.term_widths)))

    def elaborate(self, platform):
        m = Module()

        all_terms = self.terms.copy()  # create copy to avoid modifying self
        for shift in range(self.factor_width):
            part_prod = Signal(self.output.width, name=f"part_prod_{shift}")
            # construct partial product term
            mask = Repl(self.factor2[shift], self.factor_width)
            m.d.comb += part_prod.eq((self.factor1 & mask) << shift)
            all_terms.append(part_prod)

        # merge all terms together
        output = treereduce(all_terms, operator.xor)
        m.d.comb += self.output.eq(output)
        return m
