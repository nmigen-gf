# SPDX-License-Identifier: LGPL-3-or-later
# Copyright 2022 Jacob Lifshay

# Funded by NLnet Assure Programme 2021-02-052, https://nlnet.nl/assure part
# of Horizon 2020 EU Programme 957073.

import unittest
from nmigen.hdl.ast import Signal, Const, unsigned
from nmigen.hdl.dsl import Module
from nmutil.formaltest import FHDLTestCase
from nmigen_gf.hdl.cldivrem import (CLDivRemFSMStage, CLDivRemInputData,
                                    CLDivRemOutputData, CLDivRemShape,
                                    cldivrem_shifting, CLDivRemState)
from nmigen.sim import Delay, Tick
from nmutil.sim_util import do_sim, hash_256
from nmigen_gf.reference.cldivrem import cldivrem


class TestCLDivRemShifting(FHDLTestCase):
    def tst(self, shape, full):
        assert isinstance(shape, CLDivRemShape)

        def case(n, d):
            assert isinstance(n, int)
            assert isinstance(d, int)
            if d != 0:
                expected_q, expected_r = cldivrem(n, d, width=shape.width)
                q, r = cldivrem_shifting(n, d, shape)
            else:
                expected_q = expected_r = 0
                q = r = 0
            with self.subTest(expected_q=hex(expected_q),
                              expected_r=hex(expected_r),
                              q=hex(q), r=hex(r)):
                self.assertEqual(expected_q, q)
                self.assertEqual(expected_r, r)
        if full:
            for n in range(1 << shape.width):
                for d in range(1 << shape.width):
                    with self.subTest(n=hex(n), d=hex(d)):
                        case(n, d)
        else:
            for i in range(100):
                n = hash_256(f"cldivrem comb n {i}")
                n = Const.normalize(n, unsigned(shape.width))
                d = hash_256(f"cldivrem comb d {i}")
                d = Const.normalize(d, unsigned(shape.width))
                case(n, d)

    def test_6_step_1(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=1), full=True)

    def test_6_step_2(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=2), full=True)

    def test_6_step_3(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=3), full=True)

    def test_6_step_4(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=4), full=True)

    def test_6_step_6(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=6), full=True)

    def test_6_step_10(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=10), full=True)

    def test_64_step_1(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=1), full=False)

    def test_64_step_2(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=2), full=False)

    def test_64_step_3(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=3), full=False)

    def test_64_step_4(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=4), full=False)

    def test_64_step_8(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=8), full=False)


class TestCLDivRemComb(FHDLTestCase):
    def tst(self, shape, full):
        assert isinstance(shape, CLDivRemShape)
        width = shape.width
        m = Module()
        n_in = Signal(width)
        d_in = Signal(width)
        q_out = Signal(width)
        r_out = Signal(width)
        states: "list[CLDivRemState]" = []
        for i in range(shape.width + 1):
            states.append(CLDivRemState(shape, name=f"state_{i}"))
            if i == 0:
                states[i].set_to_initial(m, n=n_in, d=d_in)
            else:
                states[i].set_to_next(m, states[i - 1])
        q, r = states[-1].get_output()
        m.d.comb += [q_out.eq(q), r_out.eq(r)]

        def case(n, d):
            assert isinstance(n, int)
            assert isinstance(d, int)
            if d != 0:
                expected_q, expected_r = cldivrem_shifting(n, d, shape)
            else:
                expected_q = expected_r = 0
            with self.subTest(n=hex(n), d=hex(d),
                              expected_q=hex(expected_q),
                              expected_r=hex(expected_r)):
                yield n_in.eq(n)
                yield d_in.eq(d)
                yield Delay(1e-6)
                for i, state in enumerate(states):
                    with self.subTest(i=i):
                        done = yield state.done
                        substep = yield state.substep
                        clock = yield state.clock
                        self.assertEqual(done, i >= shape.width)
                        if i % shape.steps_per_clock == 0:
                            self.assertEqual(substep, 0)
                        if i < shape.width:
                            self.assertEqual(substep
                                             + clock * shape.steps_per_clock, i)
                q = yield q_out
                r = yield r_out
                with self.subTest(q=hex(q), r=hex(r)):
                    # only check results when inputs are valid
                    if d != 0:
                        self.assertEqual(q, expected_q)
                        self.assertEqual(r, expected_r)

        def process():
            if full:
                for n in range(1 << width):
                    for d in range(1 << width):
                        yield from case(n, d)
            else:
                for i in range(100):
                    n = hash_256(f"cldivrem comb n {i}")
                    n = Const.normalize(n, unsigned(width))
                    d = hash_256(f"cldivrem comb d {i}")
                    d = Const.normalize(d, unsigned(width))
                    yield from case(n, d)
        with do_sim(self, m, [n_in, d_in, q_out, r_out]) as sim:
            sim.add_process(process)
            sim.run()

    def test_4_step_1(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=1), full=True)

    def test_4_step_2(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=2), full=True)

    def test_4_step_3(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=3), full=True)

    def test_4_step_4(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=4), full=True)

    def test_6_step_1(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=1), full=False)

    def test_6_step_2(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=2), full=False)

    def test_6_step_6(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=6), full=False)

    def test_6_step_8(self):
        self.tst(CLDivRemShape(width=6, steps_per_clock=8), full=False)

    def test_8_step_1(self):
        self.tst(CLDivRemShape(width=8, steps_per_clock=1), full=False)

    def test_8_step_4(self):
        self.tst(CLDivRemShape(width=8, steps_per_clock=4), full=False)

    def test_8_step_8(self):
        self.tst(CLDivRemShape(width=8, steps_per_clock=8), full=False)


class TestCLDivRemFSM(FHDLTestCase):
    def tst(self, shape, full):
        assert isinstance(shape, CLDivRemShape)
        pspec = {}
        dut = CLDivRemFSMStage(pspec, shape)
        i_data: CLDivRemInputData = dut.p.i_data
        o_data: CLDivRemOutputData = dut.n.o_data
        self.assertEqual(i_data.n.shape(), unsigned(shape.width))
        self.assertEqual(i_data.d.shape(), unsigned(shape.width))
        self.assertEqual(o_data.q.shape(), unsigned(shape.width))
        self.assertEqual(o_data.r.shape(), unsigned(shape.width))

        def case(n, d):
            assert isinstance(n, int)
            assert isinstance(d, int)
            if d != 0:
                expected_q, expected_r = cldivrem(n, d, width=shape.width)
            else:
                expected_q = expected_r = 0
            with self.subTest(n=hex(n), d=hex(d),
                              expected_q=hex(expected_q),
                              expected_r=hex(expected_r)):
                yield dut.p.i_valid.eq(0)
                yield Tick()
                yield i_data.n.eq(n)
                yield i_data.d.eq(d)
                yield dut.p.i_valid.eq(1)
                yield Delay(0.1e-6)
                valid = yield dut.n.o_valid
                ready = yield dut.p.o_ready
                with self.subTest():
                    self.assertFalse(valid)
                    self.assertTrue(ready)
                yield Tick()
                yield i_data.n.eq(-1)
                yield i_data.d.eq(-1)
                yield dut.p.i_valid.eq(0)
                for step in range(shape.done_clock):
                    yield Delay(0.1e-6)
                    valid = yield dut.n.o_valid
                    ready = yield dut.p.o_ready
                    with self.subTest():
                        self.assertFalse(valid)
                        self.assertFalse(ready)
                    yield Tick()
                yield Delay(0.1e-6)
                valid = yield dut.n.o_valid
                ready = yield dut.p.o_ready
                with self.subTest():
                    self.assertTrue(valid)
                    self.assertFalse(ready)
                q = yield o_data.q
                r = yield o_data.r
                with self.subTest(q=hex(q), r=hex(r)):
                    # only check results when inputs are valid
                    if d != 0 and (expected_q >> shape.width) == 0:
                        self.assertEqual(q, expected_q)
                        self.assertEqual(r, expected_r)
                yield dut.n.i_ready.eq(1)
                yield Tick()
                yield Delay(0.1e-6)
                valid = yield dut.n.o_valid
                ready = yield dut.p.o_ready
                with self.subTest():
                    self.assertFalse(valid)
                    self.assertTrue(ready)
                yield dut.n.i_ready.eq(0)

        def process():
            if full:
                for n in range(1 << shape.width):
                    for d in range(1 << shape.width):
                        yield from case(n, d)
            else:
                for i in range(100):
                    n = hash_256(f"cldivrem fsm n {i}")
                    n = Const.normalize(n, unsigned(shape.width))
                    d = hash_256(f"cldivrem fsm d {i}")
                    d = Const.normalize(d, unsigned(shape.width))
                    yield from case(n, d)

        with do_sim(self, dut, list(dut.ports())) as sim:
            sim.add_process(process)
            sim.add_clock(1e-6)
            sim.run()

    def test_4_step_1(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=1), full=True)

    def test_4_step_2(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=2), full=True)

    def test_4_step_3(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=3), full=True)

    def test_4_step_4(self):
        self.tst(CLDivRemShape(width=4, steps_per_clock=4), full=True)

    def test_8_step_4(self):
        self.tst(CLDivRemShape(width=8, steps_per_clock=4), full=False)

    def test_64_step_4(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=4), full=False)

    def test_64_step_8(self):
        self.tst(CLDivRemShape(width=64, steps_per_clock=8), full=False)


if __name__ == "__main__":
    unittest.main()
